---
layout: page
title:  "Les cours"
tags: informatique 
author: "François Brucker"
---

## Plan

Liste des différents cours. La plupart de ceux-ci sont enseignés par bouts dans les différentes années.

## Liste

* [algorithme, code et théorie]({% link cours/algorithme-code-theorie/index.md %})
* [web]({% link cours/web/index.md %})
* [graphes]({% link cours/graphes/index.md %})
* [développement]({% link cours/developpement/index.md %})
* [introduction au code et à l'algorithmie]({% link cours/algorithmie_code/index.md %})
* [git et github]({% link cours/git_et_github/index.md %})
* [ssh et shell]({% link cours/ssh_et_shell/index.md %})
* analyse des données

> TBD
>
> * rassemebler toutes les parties traitant d'un sujet comme les tests et en faire un chapeau commun
{: .note}
