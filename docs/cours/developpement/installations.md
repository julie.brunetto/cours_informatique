---
layout: page
title:  "installations de python et vsc"
author: "François Brucker"
---

Nous allons installer et configurer les principaux outils dont nous aurons besoin pour ce cours.

## vsc

On installe l'éditeur de texte vscode :

1. Installez [vscode](https://code.visualstudio.com/)
2. commencez par vos familiariser avec grâce à [ce tutoriel]({% link _tutoriels/editeur/vsc/vsc-installation-et-prise-en-main.md %}).

On supposera par la suite que vous avez lu et fait les installations de ce tutoriel.

## installation python

Installation de l'interpréteur python (vous avons choisi la distribution [anaconda](https://www.anaconda.com/)) qui nous permettre d'exécuter nos programmes.

1. [Installez la version anaconda de python]({% link _tutoriels/python/2021-09-14-installation-anaconda.md %}).

## python avec vscode

On lie l'éditeur de texte à python :

1. Suivez le [tutoriel vscode et python]({% link _tutoriels/editeur/vsc/2021-09-14-vsc-python.md %}).
