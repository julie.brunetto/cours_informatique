---
layout: page
title:  "langage javascript"
category: cours
author: "François Brucker"
---

Ce cours, tente de donner des notions de javascript et de comment l'utiliser en console, en script ou dans un navigateur.

Le but de ce cours n'est pas de remplacer les tutoriaux du net, il y en a de très bon, mais :
1. d'introduire le sujet pour que vous puissiez comprendre  plus facilement les tutoriaux du net et séparer ceux qui sont chouettes de ceux qui ne le sont pas
2. pointer les problèmes classiques en javascript.

> TBD : en chantier
{: .note}

## Tutoriaux du net


Il y en a quelques uns de très bons. On peut citer : 

* <https://developer.mozilla.org/fr/docs/Web/JavaScript> qui contient plein de tutoriel et des ressources complète de son utilisation
* <https://javascript.info/>
* <https://grafikart.fr/tutoriels/javascript> en revanche, je ne sais pas si tout est gratuit.

Autres trucs : 
* <https://www.tutorialspoint.com/javascript/index.htm>
* <https://nodejs.org/en/>


## plan du cours

1. [bases]({% link cours/web/js/bases.md %})
2. [gestion des données]({% link cours/web/js/donnees.md %})
3. [js et le web front]({% link cours/web/js/js_et_web_front.md %})
4. [three.js pour du rendu 3D]( {% link cours/web/js/threejs.md %})
5. promise ?
6. tests ?