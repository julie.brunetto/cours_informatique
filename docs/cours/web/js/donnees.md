---
layout: page
title:  "gestion des données"
category: cours
author: "François Brucker"
---

## objet et json


Le [json](https://www.json.org/json-fr.html) (*JavaScript Object Notation*) est une façon d'écrire les objets javascript.  C'est le format par excellence de transfert de données par le web, car un objet javascript peut facilement être vu comme une donnée structurée : l'objet [ici](https://developer.mozilla.org/fr/docs/Learn/JavaScript/Objects/JSON#structure_du_json) montre les possibilités de structuration d'un objet.

Attention, le [json](https://www.json.org/json-fr.html) est du texte, ce n'est pas un objet. Les données sont ainsi :
* des objets javascript lorsqu'on les manipule
* des chaines de caractères lorsque l'on les stocke ou les transfert. 

Il faut passer par une étape de conversion pour passer de l'un à l'autre.



> TBD :
> * standard du web pour faire voyager des données.
> * c'est du texte qui est converti en objet
> * on peut l'utiliser en python mais c'est transformé en python
> * exemple d'utilisation : attention ne marche pas sans serveur.
{: .note}


## fichiers

> TBD
{: .note}

node vs web


## bases de données

sequilize