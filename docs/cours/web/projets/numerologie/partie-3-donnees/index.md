---
layout: page
title:  "Projet numérologie : partie 3 données"
category: cours
author: "François Brucker"
---

> [numérologie]({% link cours/web/projets/numerologie/index.md %}) / [partie 3]({% link cours/web/projets/numerologie/partie-3-donnees/index.md %})
{: .chemin}

Numérologie partie 3. On mets en place une petite base de données en mémoire.

## projet

1. [bases de l'utilisation des bases]({% link cours/web/projets/numerologie/partie-3-donnees/1-bases-des-bases.md %}) : comment utiliser une base de données en javascript en général et avec node/express en particulier
2. [intégration au site]({% link cours/web/projets/numerologie/partie-3-donnees/2-integration.md %})

## dépôt git final

<https://github.com/FrancoisBrucker/numerologie/releases/tag/partie-3-2-fin>
