---
layout: page
title:  "Projet numérologie : partie 2 serveur"
category: cours
author: "François Brucker"
---

> [numérologie]({% link cours/web/projets/numerologie/index.md %}) / [partie 2]({% link cours/web/projets/numerologie/partie-2-serveur/index.md %})
{: .chemin}

Numérologie partie 2. On utilise node/express pour créer un serveur web.

## prérequis

On suppose que vous avez un projet retraçant la [partie 1 / niveau 1]({% link cours/web/projets/numerologie/partie-1-front/niveau-1/5-structures.md %})

## Projet

1. [serveur web minimal]({% link cours/web/projets/numerologie/partie-2-serveur/1-serveur-web-minimal.md %}) : node comme serveur qui dit bonjour.
2. [serveur web statique]({% link cours/web/projets/numerologie/partie-2-serveur/2-serveur-web-statique.md %}) : node comme serveur qui sert des fichiers statiques.
3. [serveur web avec express]({% link cours/web/projets/numerologie/partie-2-serveur/3-serveur-web-express.md %}) : on utilise le module express pour mieux gérer les routes et les urls.
4. [intégration]({% link cours/web/projets/numerologie/partie-2-serveur/4-javascript-serveur.md %}) : on crée notre première route avec du javascript serveur et on l'appel depuis le front.
5. [Fichiers du projet final]({% link cours/web/projets/numerologie/partie-2-serveur/5-structures.md %}).

## dépôt git final

<https://github.com/FrancoisBrucker/numerologie/releases/tag/partie-2-fin>
