---
layout: page
title:  "Projet numérologie : partie 4 : jardinage"
category: cours
author: "François Brucker"
---

> [numérologie]({% link cours/web/projets/numerologie/index.md %}) / [partie 4]({% link cours/web/projets/numerologie/partie-4-jardinage/index.md %})
{: .chemin}

Dans tout projet, il y a 3 phases qu'il faut respecter, et dans cet ordre :

1. make it work
2. make it right
3. make it fast

On doit cette phrase au célèbre Kent Beck, inventeur du TDD. Ce coding mantra (comme [DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself), [KISS](https://en.wikipedia.org/wiki/KISS_principle) ou encore [YAGNI](https://en.wikipedia.org/wiki/You_aren%27t_gonna_need_it)) marche à tout niveau de votre projet, vous pouvez tout aussi bien l'appliquer à une fonction/algorithme que vous venez de développer à votre projet en entier.

Notre projet fonctionne, rendons le propre.

## plan

1. [séparation des routes]({% link cours/web/projets/numerologie/partie-4-jardinage/1-routes.md %})
2. [séparation des modèles]({% link cours/web/projets/numerologie/partie-4-jardinage/2-modeles.md %})
3. [scripts]({% link cours/web/projets/numerologie/partie-4-jardinage/3-scripts.md %})

## dépôt git final

<https://github.com/FrancoisBrucker/numerologie/releases/tag/partie-4-3-fin>
