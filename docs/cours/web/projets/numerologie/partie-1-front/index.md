---
layout: page
title:  "Projet numérologie / partie 1 : front"
category: cours
author: "François Brucker"
---

> [numérologie]({% link cours/web/projets/numerologie/index.md %}) / [partie 1]({% link cours/web/projets/numerologie/partie-1-front/index.md %})
{: .chemin}

Numérologie partie 1. On prépare notre site en créant tout ce qui est nécessaire et en le faisant fonctionner en *front*.

## prérequis

Avant de se lancer à corps perdu dans le développement et le code, vérifions que nous avons (enfin, surtout vous avez) tous les outils nécessaires :

1. et c'est le plus important **la bonne attitude** :
   * si vous ne connaissez rien : ne cédez pas à la facilité de copier/coller sans comprendre : c'est *bad karma* et ça vous rattrapera tôt ou tard. La magie — tout du moins en informatique — n'existe pas (et je suis le premier à le déplorer) : si ça fonctionne sans que vous savez au moins superficiellement pourquoi, c'est que ça n'a que l'air de fonctionner.
   * si vous connaissez déjà tout ça : faite-le tout de même cela vous permettra de suivre plus facilement les niveaux ultérieurs
2. **un cerveau** en état de marche : pour voir les correspondances, lire la doc, et poser des questions.
3. avoir **un éditeur de texte** fonctionnel. Nous utiliserons [vscode](https://code.visualstudio.com/) dans ce cours, téléchargez le et installez le.
4. un **navigateur internet** munis d'outils de développement. Tous les exemple seront fait avec [chrome](https://www.google.fr/chrome/).
5. un interpréteur javascript. On utilisera [node](https://nodejs.org/en/). Suivez le [tutoriel pour l'installation]({% link cours/web/js/bases.md %}#bloc-id-installation-node).
6. Sachez ouvrir [une fenêtre terminal]({% link _tutoriels/systeme/2021-08-24-terminal.md %})

## Niveaux

Cette partie est organisée en niveaux. Chaque niveau refait la partie en ajoutant à chaque fois des outils de développements/code de plus en plus perfectionnés. Il est recommandé de faire tous les niveaux.

1. [niveau 1]({% link cours/web/projets/numerologie/partie-1-front/niveau-1/index.md %}) : utilisation d'un éditeur de texte et bases du développement html/css/js
2. [niveau 2]({% link cours/web/projets/numerologie/partie-1-front/niveau-2/index.md %}) : ajoute d'outils de gestion de projets
3. [niveau 3]({% link cours/web/projets/numerologie/partie-1-front/niveau-3/index.md %}) : ajout de la gestion des sources avec l'application desktop de github et mise en production avec un logiciel de transfert de fichier
4. [niveau 4]({% link cours/web/projets/numerologie/partie-1-front/niveau-4/index.md %}) : ajout de la gestion des sources  et mise en production sur un site distant avec le terminal.
