---
layout: page
title:  "Projet numérologie : partie 1 / niveau 1 / mise en production"
category: cours
author: "François Brucker"
---

> [numérologie]({% link cours/web/projets/numerologie/index.md %}) / [partie 1]({% link cours/web/projets/numerologie/partie-1-front/index.md %}) / [niveau 1]({% link cours/web/projets/numerologie/partie-1-front/niveau-3/index.md %}) / [html et css]({% link cours/web/projets/numerologie/partie-1-front/niveau-3/5-mise-en-production.md %})
{: .chemin}

Mise en production.

## logiciel de transfert de fichiers

Nous alons utiliser [cyberduck](https://cyberduck.io/).

## connexion

Si vous n'avez pas de fenêtre cybeduck commencez par en créer une : *menu Fichier > nouveau Navigateur*. VOus pourrez ensuite vous conecter à un serveur distant.

1. on crée une nouvelle connexion : *menu Fichier > ouvrir une connexion*
2. On choisi le protocole "SFTP (SSH file transfer protocol)"
3. puis il ne vous reste plus qu'à remplir les autres champs.
4. connexion.

### site de l'ecm

Si vous vous êtes élève de l'ecm et que vous voulez mettre à jour votre site web :

* serveur : sas1.ec-m.fr
* login/mot de passe : le login mot de passe des salles machines

> TBD
> le perso/visible de l'école
{: .note}

## transfert des fichier

Une fois que vous vous êtes déplacé au bon endroit sur le serveur, il ne vous reste plus qu'à glisser déposer vos fichiers/dossier depuis un explorateur de fichier.
