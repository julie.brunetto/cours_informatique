---
layout: page
title:  "Projet numérologie : partie 1 / niveau 3 / préparation"
category: cours
author: "François Brucker"
---

> [numérologie]({% link cours/web/projets/numerologie/index.md %}) / [partie 1]({% link cours/web/projets/numerologie/partie-1-front/index.md %}) / [niveau 3]({% link cours/web/projets/numerologie/partie-1-front/niveau-3/index.md %}) / [préparation]({% link cours/web/projets/numerologie/partie-1-front/niveau-3/1-preparation.md %})
{: .chemin}

Initialisation du projet git.

## prérequis

Suivez les parties [installation]({% link cours/git_et_github/index.md %}#installation) et [configuration]({% link cours/git_et_github/index.md %}#configuration) du cours [git et github]({% link cours/git_et_github/index.md %}).

Vous être prêt à utiliser git.

> vous pourrez lire [le cours git]({% link cours/git_et_github/index.md %}), en particulier les usages de git, cela vous donnera une plus grande connaissance de ce que l'on va faire.

## initialisation du projet

On va utiliser l'outil [desktop](https://desktop.github.com/) de github.

Une fois l'application lancée, connectez vous, puis créez un nouveau projet (*File > New repository*) : choisissez son nom (*numerologie*), puis cliquez sur *publish repository*.

## ajout du fichier

Suivez la partie [préparation du niveau 1]({% link cours/web/projets/numerologie/partie-1-front/niveau-1/1-preparation.md %}) jusqu'à la fin.

## premier commit

La fenêtre de l'application desktop doit ressembler à ça :

![usage github]({{ "/assets/cours/web/numerologie/partie-1-niveau-3-desktop.png" | relative_url }}){:style="margin: auto;display: block}

Le message de commit est pré-rempli. Commitons tout ça en appuyant sur le bouton. On doit alors obtenir une application ainsi :

![usage github]({{ "/assets/cours/web/numerologie/partie-1-niveau-3-desktop-post-commit.png" | relative_url }}){:style="margin: auto;display: block}

Il n'y a rien à faire localement. Profitons-en pour publier notre fichier sur github : cliquez sur le bouton *publish repository*

* choisissez un nomde dossier *numerologie*
* choissez de rendre ce projet public en décochant la checkbox

Puis cliquer sur le bouton *publish repository*. Vous pouvez aller sur github pour voir votre nouveau répertoire.
