---
layout: page
title:  "Projet numérologie / partie 1 / niveau 3 : git, github et mise en production"
category: cours
author: "François Brucker"
---

> [numérologie]({% link cours/web/projets/numerologie/index.md %}) / [partie 1]({% link cours/web/projets/numerologie/partie-1-front/index.md %}) / [niveau 3]({% link cours/web/projets/numerologie/partie-1-front/niveau-3/index.md %})
{: .chemin}

Numérologie niveau 3. On ajoute l'utilisation (basique) d'un gestionnaire de sources (git), et mon met en production le site sur un serveur distant

## prérequis

## plan

1. [préparation du projet]({% link cours/web/projets/numerologie/partie-1-front/niveau-3/1-preparation.md %})
2. [code-js]({% link cours/web/projets/numerologie/partie-1-front/niveau-3/2-code-js.md %})
3. [html]({% link cours/web/projets/numerologie/partie-1-front/niveau-3/3-html_css.md %})
4. [intégration du js]({% link cours/web/projets/numerologie/partie-1-front/niveau-3/4-integration_html_js.md %})
5. [mise en production]({% link cours/web/projets/numerologie/partie-1-front/niveau-3/5-mise-en-production.md %})

