---
layout: page
title:  "Projet numérologie / partie 1 / niveau 4 : git et github en lignes de commande"
category: cours
author: "François Brucker"
---

> [numérologie]({% link cours/web/projets/numerologie/index.md %}) / [partie 1]({% link cours/web/projets/numerologie/partie-1-front/index.md %}) / [niveau 4]({% link cours/web/projets/numerologie/partie-1-front/niveau-4/index.md %})
{: .chemin}

Numérologie niveau 4. On ajoute l'utilisation (basique) d'un gestionnaire de sources (git) en utilisant les lignes de commandes.

## plan

1. [préparation du projet]({% link cours/web/projets/numerologie/partie-1-front/niveau-4/1-preparation.md %})
2. [code-js]({% link cours/web/projets/numerologie/partie-1-front/niveau-4/2-code-js.md %})
3. [html]({% link cours/web/projets/numerologie/partie-1-front/niveau-4/3-html_css.md %})
4. [intégration du js]({% link cours/web/projets/numerologie/partie-1-front/niveau-4/4-integration_html_js.md %})
