---
layout: page
title:  "Projet numérologie : niveau 2/partie 1 : front"
category: cours
author: "François Brucker"
---

> [numérologie]({% link cours/web/projets/numerologie/index.md %}) / [partie 1]({% link cours/web/projets/numerologie/partie-1-front/niveau-2/index.md %}) / [niveau 2]({% link cours/web/projets/numerologie/partie-1-front/niveau-2/index.md %})
{: .chemin}

Numérologie niveau 2. On ajoute une partie gestion de projet avec des des todos, une user story, et un balbutiement de tests.

## plan

1. [préparation du projet]({% link cours/web/projets/numerologie/partie-1-front/niveau-2/1-preparation.md %})
2. [code js]({% link cours/web/projets/numerologie/partie-1-front/niveau-2/2-code_js.md %})
3. [code html/css]({% link cours/web/projets/numerologie/partie-1-front/niveau-2/3-html_css.md %})
4. [intégration js et html]({% link cours/web/projets/numerologie/partie-1-front/niveau-2/4-integration_html_js.md %})

[Fichiers du projet final]({% link cours/web/projets/numerologie/partie-1-front/niveau-2/5-structures.md %}).
