---
layout: page
title:  "Projet numérologie : partie 1 : front / niveau 1"
category: cours
author: "François Brucker"
---

> [numérologie]({% link cours/web/projets/numerologie/index.md %}) / [partie 1]({% link cours/web/projets/numerologie/partie-1-front/index.md %}) / [niveau 1]({% link cours/web/projets/numerologie/partie-1-front/niveau-1/index.md %})
{: .chemin}

Numérologie partie 1 - niveau 1. On ne verra que le strict nécessaire pour pour faire fonctionner le code :

* utilisation basique d'un éditeur de texte performant, ici [vscode](https://code.visualstudio.com/),
* stricte minimum de html/css pour pouvoir mettre en oeuvre notre serveur web,
* on tentera de s'amuser en javascript sans rentrer trop dans les détails de fonctionnement.

## plan

1. [préparation du projet]({% link cours/web/projets/numerologie/partie-1-front/niveau-1/1-preparation.md %})
2. [code js]({% link cours/web/projets/numerologie/partie-1-front/niveau-1/2-code_js.md %})
3. [code html/css]({% link cours/web/projets/numerologie/partie-1-front/niveau-1/3-html_css.md %})
4. [intégration js et html]({% link cours/web/projets/numerologie/partie-1-front/niveau-1/4-integration_html_js.md %})

[Fichiers du projet final]({% link cours/web/projets/numerologie/partie-1-front/niveau-1/5-structures.md %}).
