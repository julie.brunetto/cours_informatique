---
layout: page
title:  "Algorithme, code et théorie : théorie"
category: cours
---

> [Algorithme, code et théorie]({% link cours/algorithme-code-theorie/index.md %}) / [théorie]({% link cours/algorithme-code-theorie/theorie/index.md %})
{: .chemin}

Que peut ou pas faire un algorithme.

> *L'informatique n'est pas plus la science des ordinateurs que l'astronomie n'est celle des télescopes* [E. Dijkstra](https://fr.wikipedia.org/wiki/Edsger_Dijkstra)
{: .note}

* [fonctions]({% link cours/algorithme-code-theorie/theorie/fonctions.md %})
* [Algorithmes, fonctions et pseudo-code]({% link cours/algorithme-code-theorie/theorie/algorithmes-fonctions-pseudo-code.md %})
* [machine de Turing]({% link cours/algorithme-code-theorie/theorie/machine-turing.md %})
* [décidabilité]({% link cours/algorithme-code-theorie/theorie/decidabilite.md %})
* [calculabilité]({% link cours/algorithme-code-theorie/theorie/calculabilite.md %})
* [fonctions de hash]({% link cours/algorithme-code-theorie/theorie/fonctions-hash.md %})

* logique = formule logique = sat
* problème de décision : sous ensemble vrai d'un ensemble. se dérive de langage décidable

> ref à remanier propre)
{: .tbd}

## refs

poly de Pascal.

<https://en.wikipedia.org/wiki/List_of_undecidable_problems>
<https://plato.stanford.edu/entries/church-turing/>
<http://pageperso.lif.univ-mrs.fr/~kevin.perrot/documents/2016/calculabilite/Cours_16.pdf>
<https://www.cs.odu.edu/~zeil/cs390/latest/Public/turing-complete/index.html>