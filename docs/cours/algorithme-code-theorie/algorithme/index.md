---
layout: page
title:  "Algorithme, code et théorie : algorithmie"
category: cours
---

> [Algorithme, code et théorie]({% link cours/algorithme-code-theorie/index.md %}) / [algorithmie]({% link cours/algorithme-code-theorie/algorithme/index.md %})
{: .chemin}

Création, preuve et complexité des algorithmes.

* [algorithmes ?]({% link cours/algorithme-code-theorie/algorithme/algorithmes.md %})
* [pseudo-code]({% link cours/algorithme-code-theorie/algorithme/pseudo-code.md %})
* [complexité max/min]({% link cours/algorithme-code-theorie/algorithme/complexite-max-min.md %})
* [preuve d'algorithme]({% link cours/algorithme-code-theorie/algorithme/preuve-algorithme.md %})
* [étude : l'exponentiation]({% link cours/algorithme-code-theorie/algorithme/etude-exponentiation.md %})
* [complexité en moyenne]({% link cours/algorithme-code-theorie/algorithme/complexite-moyenne.md %})
* [complexité d'un problème]({% link cours/algorithme-code-theorie/algorithme/complexite-probleme.md %})
* [étude : mélanger un tableau]({% link cours/algorithme-code-theorie/algorithme/etude-melange.md %})
* [étude : trier un tableau]({% link cours/algorithme-code-theorie/algorithme/etude-tris.md %})
* [structure : dictionnaire]({% link cours/algorithme-code-theorie/algorithme/structure-dictionnaire.md %})
* [structure : liste]({% link cours/algorithme-code-theorie/algorithme/structure-liste.md %})
* [algorithmes gloutons]({% link cours/algorithme-code-theorie/algorithme/structure-liste.md %})
* [étude : voyageur de commerce]({% link cours/algorithme-code-theorie/algorithme/etude-voyageur-de-commerce.md %})
* [structure : chaine de caractères]({% link cours/algorithme-code-theorie/algorithme/structure-chaine-de-caracteres.md %})
* [étude : recherche de sous-chaines]({% link cours/algorithme-code-theorie/algorithme/etude-recherche-sous-chaines.md %})
* [étude : alignement de séquences]({% link cours/algorithme-code-theorie/algorithme/etude-alignement-sequences.md %})
