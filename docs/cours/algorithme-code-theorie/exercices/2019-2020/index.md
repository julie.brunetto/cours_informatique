---
layout: page
title:  "exercices : 2019/2020"
category: cours
---

> [Algorithme, code et théorie]({% link cours/algorithme-code-theorie/index.md %}) / [exercices]({% link cours/algorithme-code-theorie/exercices/index.md %}) / [2019-2020]({% link cours/algorithme-code-theorie/exercices/2019-2020/index.md %})
{: .chemin}

Examens de l'année scolaire 2019/2020

## examens

1. [DS (sujet)](./ds.pdf)
2. [examen (sujet)](./mpci_devoir_2019_2020.pdf)
