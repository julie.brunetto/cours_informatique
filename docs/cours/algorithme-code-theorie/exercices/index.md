---
layout: page
title:  "Algorithme, code et théorie : exercices"
category: cours
---

> [Algorithme, code et théorie]({% link cours/algorithme-code-theorie/index.md %}) / [exercices]({% link cours/algorithme-code-theorie/exercices/index.md %})
{: .chemin}

Listes d'exercices, tests et examens données pour ce cours.

1. [2021/2022]({% link cours/algorithme-code-theorie/exercices/2021-2022/index.md %})
2. [2020/2021]({% link cours/algorithme-code-theorie/exercices/2020-2021/index.md %})
3. [2019/2020]({% link cours/algorithme-code-theorie/exercices/2019-2020/index.md %})
