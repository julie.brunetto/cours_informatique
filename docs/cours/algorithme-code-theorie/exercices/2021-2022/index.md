---
layout: page
title:  "exercices : 2021/2022"
category: cours
---

> [Algorithme, code et théorie]({% link cours/algorithme-code-theorie/index.md %}) / [exercices]({% link cours/algorithme-code-theorie/exercices/index.md %}) / [2021-2022]({% link cours/algorithme-code-theorie/exercices/2021-2022/index.md %})
{: .chemin}

Tests et examens pour l'année scolaire 2021/2022

## Tests

1. code : [sujet]({% link cours/algorithme-code-theorie/exercices/2021-2022/1_test_sujet.md %}) et [corrigé]({% link cours/algorithme-code-theorie/exercices/2021-2022/1_test_corrige.md %})
2. complexité et preuve : [sujet]({% link cours/algorithme-code-theorie/exercices/2021-2022/2_test_sujet.md %}) et [corrigé]({% link cours/algorithme-code-theorie/exercices/2021-2022/2_test_corrige.md %})
3. DS 1 : [sujet]({% link cours/algorithme-code-theorie/exercices/2021-2022/3_ds_sujet.md %}) et [corrigé]({% link cours/algorithme-code-theorie/exercices/2021-2022/3_ds_corrige.md %})
4. classes et objets : [sujet]({% link cours/algorithme-code-theorie/exercices/2021-2022/4_test_sujet.md %}) et [corrigé]({% link cours/algorithme-code-theorie/exercices/2021-2022/4_test_corrige.md %})
5. classes et objets (le retour) : [sujet]({% link cours/algorithme-code-theorie/exercices/2021-2022/5_test_sujet.md %}) et [corrigé]({% link cours/algorithme-code-theorie/exercices/2021-2022/5_test_corrige.md %})
6. DS 2 : [sujet]({% link cours/algorithme-code-theorie/exercices/2021-2022/ds_2_sujet.md %}) et [corrigé]({% link cours/algorithme-code-theorie/exercices/2021-2022/ds_2_corrige.md %}) 
7. algorithmes gloutons : [sujet]({% link cours/algorithme-code-theorie/exercices/2021-2022/6_test_sujet.md %}) et [corrigé]({% link cours/algorithme-code-theorie/exercices/2021-2022/6_test_corrige.md %})
8. examen terminal : [sujet]({% link cours/algorithme-code-theorie/exercices/2021-2022/7_et_sujet.md %})
