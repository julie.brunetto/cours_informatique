---
layout: page
title:  "corrigé DS 2 : projet de code"
category: cours
tags: code python
---

> [Algorithme, code et théorie]({% link cours/algorithme-code-theorie/index.md %}) / [exercices]({% link cours/algorithme-code-theorie/exercices/index.md %}) / [2021-2022]({% link cours/algorithme-code-theorie/exercices/2021-2022/index.md %}) / [corrige DS 2 : projet de code]({% link cours/algorithme-code-theorie/exercices/2021-2022/ds_2_corrige.md %})
{: .chemin}


Un corrigé possible, sans tests, de la partie obligatoire du DS est disponible à [cet endroit](https://github.com/FrancoisBrucker/cours_informatique/tree/master/docs/cours/algorithme-code-theorie/exercices/2021-2022/ds_2_code).

Une fois les 4 fichiers téléchargés, vous pouvez lancer le programme avec la commande `python main.py`.
