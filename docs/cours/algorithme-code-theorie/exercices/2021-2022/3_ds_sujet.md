---
layout: page
title:  "DS"
category: cours
tags: informatique cours 
authors: "François Brucker"
---

> [Algorithme, code et théorie]({% link cours/algorithme-code-theorie/index.md %}) / [exercices]({% link cours/algorithme-code-theorie/exercices/index.md %}) / [2021-2022]({% link cours/algorithme-code-theorie/exercices/2021-2022/index.md %}) / [sujet DS]({% link cours/algorithme-code-theorie/exercices/2021-2022/3_ds_sujet.md %})
{: .chemin}

[sujet](./ds_1_2021_2022.pdf)

Un **DS de 2h** sur la complexité et l'écriture d'algorithmes simple.
