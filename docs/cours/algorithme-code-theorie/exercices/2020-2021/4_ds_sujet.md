---
layout: page
title:  "DS"
category: cours
tags: informatique cours 
authors: "François Brucker"
---


Ce **DS de 2h** correspond aux 7 premières questions (environ 1/3) de cet [examen de concours 2019](./informatique_B_2019.pdf).

> Notez que pour le concours, le temps total alloué pour la totalité de l'épreuve était également de 2h, il fallait donc faire vite et bien. Pendant le DS, vous aviez plus de temps pour réfléchir.
