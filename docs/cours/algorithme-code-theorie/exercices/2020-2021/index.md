---
layout: page
title:  "exercices : 2020/2021"
category: cours
---

> [Algorithme, code et théorie]({% link cours/algorithme-code-theorie/index.md %}) / [exercices]({% link cours/algorithme-code-theorie/exercices/index.md %}) / [2020-2021]({% link cours/algorithme-code-theorie/exercices/2020-2021/index.md %})
{: .chemin}

Tests et examens pour l'année scolaire 2020/2021

## Tests

1. code : [sujet]({% link cours/algorithme-code-theorie/exercices/2020-2021/1_test_sujet.md %}) et [corrigé]({% link cours/algorithme-code-theorie/exercices/2020-2021/1_test_corrige.md %})
2. complexité : [sujet]({% link cours/algorithme-code-theorie/exercices/2020-2021/2_test_sujet.md %}) et [corrigé]({% link cours/algorithme-code-theorie/exercices/2020-2021/2_test_corrige.md %})
3. preuve : [sujet]({% link cours/algorithme-code-theorie/exercices/2020-2021/3_test_sujet.md %}) et [corrigé]({% link cours/algorithme-code-theorie/exercices/2020-2021/3_test_corrige.md %})
4. modélisation : [sujet]({% link cours/algorithme-code-theorie/exercices/2020-2021/5_test_sujet.md %}) et [corrigé]({% link cours/algorithme-code-theorie/exercices/2020-2021/5_test_corrige.md %})
5. gloutons : [sujet]({% link cours/algorithme-code-theorie/exercices/2020-2021/6_test_sujet.md %}) et [corrigé]({% link cours/algorithme-code-theorie/exercices/2020-2021/6_test_corrige.md %})

## DS

1. DS : [sujet]({% link cours/algorithme-code-theorie/exercices/2020-2021/4_ds_sujet.md %}) et [corrigé]({% link cours/algorithme-code-theorie/exercices/2020-2021/4_ds_corrige.md %})
2. [examen terminal (sujet)](./mpci_et_2020_2021.pdf)
