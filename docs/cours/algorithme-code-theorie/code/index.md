---
layout: page
title:  "Algorithme, code et théorie : coder"
category: cours
---

> [Algorithme, code et théorie]({% link cours/algorithme-code-theorie/index.md %}) / [code]({% link cours/algorithme-code-theorie/code/index.md %})
{: .chemin}

Comment coder, maintenir et aimer ses programmes. Les projets constituent une progression, il est conseillé de les suivre dans l'ordre.

1. [coder]({% link cours/algorithme-code-theorie/code/coder.md %})
2. [projet : hello dev]({% link cours/algorithme-code-theorie/code/projet-hello-dev.md %})
3. [projet : pourcentages]({% link cours/algorithme-code-theorie/code/projet-pourcentages.md %})
4. [projet : exponentiation]({% link cours/algorithme-code-theorie/code/projet-exponentiation.md %})
5. [projet : tris]({% link cours/algorithme-code-theorie/code/projet-tris.md %})
6. [mémoire et espace de noms]({% link cours/algorithme-code-theorie/code/memoire-et-espace-noms.md %})
7. [programmation objets]({% link cours/algorithme-code-theorie/code/programmation-objet/index.md %})
8. [projet : programmation événementielle]({% link cours/algorithme-code-theorie/code/projet-programmation-evenementielle.md %})
9. [fichiers]({% link cours/algorithme-code-theorie/code/fichiers.md %})
10. [projet : alignement de séquences]({% link cours/algorithme-code-theorie/code/projet-alignement-sequences.md %})

## tbd

ordre des concepts de code :

1. semaine 7 : poetry
2. semaine 7 : venv
3. dear pygui ?