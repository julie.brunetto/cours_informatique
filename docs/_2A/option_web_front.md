---
layout: page
title:  "Option : web front"
category: cours
---


Cours de développement web consacré au [web front](https://fr.wikipedia.org/wiki/D%C3%A9veloppement_web_frontal).

Pour majeure partie en *classe inversée*[^1], les élèves réaliseront en groupe plusieurs projets et présentations/cours/tutos de technologies web selon leurs connaissances et leur appétence.

[^1]: Une classe inversée, c'est le contraire d'une classe normale, c'est à dire là c'est les élèves qui travaillent et c'est le prof fait semblant d'être intéressé.

> Un bout du [cours web global]({% link cours/web/index.md %})

## plan

> TBD
{: .note}


### compétences

Tout le monde doit arriver niveau 3.

#### niveau I

1. outils de développement chrome
2. html/css dans une page
   * p 
   * strong, em
   * img
   * a et pseudo-css
3. js dans une page html
   * élément de langage
   * liens onclick/onmove
   * arbre dom 
4. liens js/css depuis :
   * local
5. site de référence css/html

#### niveau 2

1. plugin css/js (bootstrap, pure-css, bulma ?)
   * cdn
   * node_module (avec `npm`)
2. div, span
   * class et id
   * ajout de style avec l'attribut style
3. js comme langage de script
   * comprendre un script
   * utilisation dans un node

#### niveau 3

1. bibliothèque complète : 3djs
2. production de html depuis un autre langage comme markdown (avec )
3. créer un site jekyll simple
4. pouvoir modifier le site <https://francoisbrucker.github.io/cours_informatique/> en ajoutant ses propres cours/tutos.

#### niveau 4

1. jekyll niveau 2 : comprendre entièrement le site
2. scss
3. produire du html avec un template
   * mustache
   * liquid avec jekyll

#### niveau 5

1. parcell et webpack pour faire son propre site statique
2. ajout de postcss et tailwindcss

#### niveau 6

1. UI avec vuejs.

## Notation

La notation est en 3 parties :

* On notera le travail fait pendant les séances (la présence bien que nécessaire ne sera suffisante pour avoir une bonne note).
* on notera les projets finis
* on notera les présentations/tutos/cours de technologies spécifiques

* html/css
* js
* UI avec vuejs
* web static avec jekyll
* sass
* postcss
* import static/DCN/node_modules