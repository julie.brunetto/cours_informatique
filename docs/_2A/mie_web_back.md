---
layout: page
title:  "MIE I1 : serveur web"
category: cours
---

## Introduction

> Partie I1 du [programme MIE](https://docs.google.com/document/d/19BjB7vXDtT0gcqS45Z7Ai_G1_S0hFj-Cqv2f78YUy9M)

Cours de développement web. On y verra ce qu'est un serveur web et comment en construire un en [node](https://nodejs.org/en/).

## Prérequis

Les connaissances et les outils que vous devez avoir pour commencer le cours.

### ordinateur pour le développement

Vous devrez écrire du code et installer des applications durant ce cours. Vous devez donc avoir un ordinateur portable en état de marche et des connaissance minimale sur son fonctionnement.

Plus précisément, peu importe le système d'exploitation (Linux, Mac ou windows) vous devez :

* avoir un système sain. Dans le doute [faire une installation fraîche de tout votre système]({% link _tutoriels/systeme/2021-09-01-installation-ordinateur.md %})
* connaitre les bases d'un système d'exploitation, [les fichiers et les dossiers]({% link _tutoriels/systeme/fichiers-navigation.md %})
* avoir accès à un [terminal]({% link _tutoriels/systeme/2021-08-24-terminal.md %})

### bases en programmation

Vous devez avoir déjà utilisé un langage de programmation, idéalement python

## Organisation

### semaine type

En trois parties :

* cours  : on verra sous la forme d'un tutoriel/projet
* autonomie : préparation du TD ou travail sur le TD
* TD : vous travaillez sur votre propre projet, qui suit la trame du projet du cours.

### plan

Chaque semaine est dédiée à un thème particulier :

1. javascript dans un navigateur et avec node
2. qu'est-ce qu'un serveur web et on en crée un avec [node](https://nodejs.org/en/) qu'on améliore en utilisant [express](https://expressjs.com/fr/)
3. on ajoute une base de donnée qu'on utilise grâce à [sequilize](https://sequelize.org/).
4. présentation de vos projets.

## Note

* on notera la présence ainsi que votre coordination cerveau/clavier lors des TDs.
* lors de travail en autonomie, on vérifiera votre travail en début de séance suivante.
* une présentation finale de vos projets aura lieu en fin d'UE

## Semaines

> Semaine 1 à 4 du [planning MIE](https://docs.google.com/spreadsheets/d/1XwjeAgwijaYZJEHFg-t_RHMXgi2Qa3d1)

### cours/autonomie

L'autonomie consistera à refaire le cours (le niveau 1) et à faire le cours (niveau 2).

### TD

Le TD consistera **toujours** à reprendre le cours de niveau 2 de la semaine et à l'adapter pour le projet de son groupe.

> * règles :
>   * sur le site du créateur : <https://www.knizia.de/wp-content/uploads/reiner/freebies/Website-Decathlon.pdf>
>   * en français : <http://www.jeuxprintandplay.fr/Fiches%20jeux/Fiche%20jeu%20Decathlon.html>
> * supports pour y jouer en vrai : <http://juegosrollandwrite.com/remake-reiner-knizias-decathlon/>

### Semaine 1

> TBD :
> à refaire en faisant comme fil rouge 1 jeu du décathlon que les élèves ne pourront pas choisir.
{: .note}

Projet [numérologie]({% link cours/web/projets/numerologie/index.md %}), [partie 1]({% link cours/web/projets/numerologie/partie-1-front/index.md %})

1. Lundi 8-10 : cours (niveau 1)
2. Lundi 10-12 : autonomie (faites le niveau 2)
3. Mercredi 8-10 : TD (votre site)

### Semaine 2

Projet [numérologie]({% link cours/web/projets/numerologie/index.md %}), [partie 2]({% link cours/web/projets/numerologie/partie-2-serveur/index.md %})

1. Lundi 8-10 : cours
2. Lundi 10-12 : TD (refaite le cours avec votre projet)
3. Mercredi 8-10 : autonomie : on finalise le TD

### Semaine 3

Projet [commentaires]({% link cours/web/projets/commentaires/index.md %})

1. Lundi 8-10 : autonomie : on finalise le TD
2. Lundi 10-12 : cours
3. Mercredi 8-10 : TD

### semaine 4

> prérequis : vous avez fini votre projet [vous le présenterez]

1. Lundi 8-10 : autonomie (finalise votre projet)
2. Lundi 10-12 : autonomie (finalise votre projet)
3. Mercredi 8-10 : présentation projets

## Pour aller plus loin

Vous pouvez toujours regarder le [cours complet]({% link cours/web/index.md %}).
