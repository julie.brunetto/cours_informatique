---
layout: page
title: About
permalink: /about/
---


| ![le truand]({{ "/assets/kame_sennin.jpg" | relative_url }}) | Regroupements de cours d'informatique donné tout au long de l'année et de la formation sous la direction de François Brucker, Prof à l'école centrale Marseille. |
|| mél : *letruand@centrale-marseille.fr* |

- [le github du site](https://github.com/FrancoisBrucker)
- [ma page web pas du tout informative](https://fbrucker.perso.centrale-marseille.fr/)


