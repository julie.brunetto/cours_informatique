---
layout: page
title:  "Centrale Casablanca : programmation objet en python"
category: cours
---

## Introduction

Cours de programmation objet.

Est organisée en 3 types d'enseignements :

* séance d'autonomie (**AU**)
  > pour s'auto-former
* séance de cours (**CM**)
  > pour voir de nouvelles connaissances
* séance machine (**SM**) : exercices fait avec votre machine, accompagné d'un prof ou d'un corrigé
  > pour travailler/acquérir des connaissances pratiques

## Séances

Huit séances sont programmées.

### prérequis (**AU**)

Première séance, en autonomie. Suivez les différents tutoriaux de la partie [première partie]({% link cours/developpement/index.md %}#partie-1) du cours de développement.

* **indispensable** : vous devez avoir suivi la partie [bases]({% link cours/developpement/index.md %}#partie-1.1)
* **pour aller plus loin** : vous devez avoir suivi la partie [méthode de développement]({% link cours/developpement/index.md %}#partie-1.2) est là pour vous faire progresser dans la connaissance du développement informatique, si vous connaissiez déjà le langage python.

A la fin de cette partie vous aurez un éditeur de texte et un interpréteur python fonctionnels pour suivre les différents TPs.

### séance 2 (**CM**)

Cours sur les objets et les classes et comment on les code en python.

1. [classes et objets]({% link cours/developpement/programmation-objet/classes-et-objets.md %})

### séance 3 (**CM**)

Comment composer et agréger les objets pour créer des objets plus fonctionnels :

1. [composition et agrégation]({% link cours/developpement/programmation-objet/composition-agregation.md %})

### séance 4 (**SM**)

Exercice de modélisation objet en python :

1. [exercices]({% link cours/developpement/programmation-objet/objets-composition-agregation-exercices.md %})
2. [corrigé]({% link cours/developpement/programmation-objet/objets-composition-agregation-exercices-corrige.md %})

### séance 5 (**CM**)

1. [héritage en programmation objet]({% link cours/developpement/programmation-objet/heritage.md %})

### séance 6 (**SM**)

1. [exercices sur l'héritage]({% link cours/developpement/programmation-objet/heritage-exercices.md %})
Codons des classes qui héritent les unes des autres
2. [corrigé]({% link cours/developpement/programmation-objet/heritage-exercices-corrige.md %})

### séance 7 (**SM**)

1. [programmation événementielle et interface graphique]({% link cours/developpement/programmation-objet/programmation-evenementielle.md %})
2. [corrigé]({% link cours/developpement/programmation-objet/programmation-evenementielle-corrige.md %})

### séance 8 (**SM**)

La **séance machine est notée**.

## note

**Le TP 8 sera noté.** Il faudra le rendre une quinzaine de jours plus tard (la date vous sera communiquée).
