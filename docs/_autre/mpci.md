---
layout: page
title:  "MPCI S2 : Programmation et Algorithmes"
category: cours
---

Cours de programmation en S2 MPCI. Il suit [le cours du S1](https://ametice.univ-amu.fr/course/view.php?id=77318) qu'il considère acquis.

Les divers contrôles intermédiaires seront à rendre directement sur [ametice](https://ametice.univ-amu.fr/course/view.php?id=87451).

> Pour que tout se passe au mieux, il est recommandé de partir d'une installation fraiche de votre système (pour éviter les $N \gg 1$ versions différentes de python par exemple).
>
> Un tuto pour installer proprement un ordinateur pour le développement est [disponible ici](({% link _tutoriels/systeme/2021-09-01-installation-ordinateur.md %})).
>
> Sinon demandez au prof, il vous aidera à bien faire les choses.
{: .attention}

## Plan des cours

L'ensemble du cours (et d'autres choses) est disponible [là]({% link cours/algorithme-code-theorie/index.md %})

> Chaque **vendredi** à partir de la semaine 2 il y aura un contrôle de 15min en début de séance portant sur le programme de la semaine précédente.
{: .attention}

### semaine 1

#### mercredi {#m-1}

1. [un algorithme ?]({% link cours/algorithme-code-theorie/algorithme/algorithmes.md %})
2. [pseudo-code]({% link cours/algorithme-code-theorie/algorithme/pseudo-code.md %})
3. [fonctions]({% link cours/algorithme-code-theorie/theorie/fonctions.md %})
4. [Algorithmes, fonctions et pseudo-code]({% link cours/algorithme-code-theorie/theorie/algorithmes-fonctions-pseudo-code.md %})

#### vendredi {#v-1}

1. [coder]({% link cours/algorithme-code-theorie/code/coder.md %})
2. projet 1 : mise en œuvre d'un projet informatique
   * prérequis :
     1. [naviguer dans un système de fichiers]({% link _tutoriels/systeme/fichiers-navigation.md %})
     2. [vscode et python]({% link _tutoriels/editeur/vsc/2021-09-14-vsc-python.md %})
   * [sujet]({% link cours/algorithme-code-theorie/code/projet-hello-dev.md %})
3. projet 2 : pourcentages
   * prérequis :
     1. [terminal]({% link _tutoriels/systeme/2021-08-24-terminal.md %})
     2. [utilisation du terminal]({% link _tutoriels/systeme/2021-12-02-terminal-utilisation.md %})
   * [sujet]({% link cours/algorithme-code-theorie/code/projet-pourcentages.md %})

### semaine 2

> Le contrôle de vendredi portera sur la partie code. Il faudra rendre un (ou plusieurs) fichiers python sur amétice.

#### mercredi {#m-2}

1. [preuve d'algorithme]({% link cours/algorithme-code-theorie/algorithme/preuve-algorithme.md %})
2. [complexité max/min]({% link cours/algorithme-code-theorie/algorithme/complexite-max-min.md %})

#### vendredi {#v-2}

1. [étude : l'exponentiation]({% link cours/algorithme-code-theorie/algorithme/etude-exponentiation.md %})
2. [projet : l'exponentiation]({% link cours/algorithme-code-theorie/code/projet-exponentiation.md %})
3. Pour aller plus loin : [étude : mélanger un tableau]({% link cours/algorithme-code-theorie/algorithme/etude-melange.md %})

### semaine 3

> Le contrôle de vendredi portera sur la partie complexité et preuve d'algorithme. Il faudra rendre une copie.

#### mercredi {#m-3}

1. [complexité en moyenne]({% link cours/algorithme-code-theorie/algorithme/complexite-moyenne.md %})
2. [complexité d'un problème]({% link cours/algorithme-code-theorie/algorithme/complexite-probleme.md %})
3. [étude : trier un tableau]({% link cours/algorithme-code-theorie/algorithme/etude-tris.md %}) (début)

#### vendredi {#v-3}

1. [étude : trier un tableau]({% link cours/algorithme-code-theorie/algorithme/etude-tris.md %}) (fin)
2. [projet : tris]({% link cours/algorithme-code-theorie/code/projet-tris.md %})
3. Pour aller plus loin : [étude : mélanger un tableau]({% link cours/algorithme-code-theorie/algorithme/etude-melange.md %})

### semaine 4

contrôle 1 : sur table. [sujet]({% link cours/algorithme-code-theorie/exercices/2021-2022/3_ds_sujet.md %})

Au programme :

* complexité (min/max/moyenne/problème)
* preuve d'algorithmes
* algorithmes de tris

### semaine 5

> Pas de contrôle.

#### mercredi {#m-5}

1. [mémoire et espace de noms]({% link cours/algorithme-code-theorie/code/memoire-et-espace-noms.md %})
2. [classes et objets]({% link cours/algorithme-code-theorie/code/programmation-objet/classes-et-objets.md %})

#### vendredi {#v-5}

1. [composition et agrégation]({% link cours/algorithme-code-theorie/code/programmation-objet/composition-agregation.md %})
2. [projet : composition et agrégation]({% link cours/algorithme-code-theorie/code/programmation-objet/projet-composition-agregation.md %})

### semaine 6

> A partir de la semaine 6, les contrôles de début séance seront à écrire en [markdown]({% link _tutoriels/format-markdown.md %}) et envoyé converti en html (ou pdf) sur amétice.
{: .attention}

#### mercredi {#m-6}

1. corrigé du [projet : composition et agrégation]({% link cours/algorithme-code-theorie/code/programmation-objet/projet-composition-agregation.md %})
2. [héritage]({% link cours/algorithme-code-theorie/code/programmation-objet/heritage.md %}) (début)

#### vendredi {#v-6}

1. [héritage]({% link cours/algorithme-code-theorie/code/programmation-objet/heritage.md %}) (fin)
2. [projet : héritage]({% link cours/algorithme-code-theorie/code/programmation-objet/projet-heritage.md %})
3. pour aller plus loin : [projet : TDD]({% link cours/algorithme-code-theorie/code/programmation-objet/projet-tdd.md %})

### semaine 7

#### mercredi {#m-7}

1. [structure : liste]({% link cours/algorithme-code-theorie/algorithme/structure-liste.md %})
2. [fonctions de hash]({% link cours/algorithme-code-theorie/theorie/fonctions-hash.md %})
3. [structure : dictionnaire]({% link cours/algorithme-code-theorie/algorithme/structure-dictionnaire.md %})

#### vendredi {#v-7}

1. [projet : programmation événementielle]({% link cours/algorithme-code-theorie/code/projet-programmation-evenementielle.md %})

### semaine 8

contrôle 2 : sur ordinateur. [sujet]({% link cours/algorithme-code-theorie/exercices/2021-2022/ds_2_sujet.md %})

Au programme :

* code

### semaine 9

> Pas de contrôle.

#### mercredi {#m-9}

1. [algorithmes gloutons]({% link cours/algorithme-code-theorie/algorithme/methode-gloutons.md %})

#### vendredi {#v-9}

1. [étude : voyageur de commerce]({% link cours/algorithme-code-theorie/algorithme/etude-voyageur-de-commerce.md %})

### semaine 10

#### mercredi {#m-10}

1. [structure : chaine de caractères]({% link cours/algorithme-code-theorie/algorithme/structure-chaine-de-caracteres.md %})
2. [fichiers]({% link cours/algorithme-code-theorie/code/fichiers.md %})

#### vendredi {#v-10}

1. [projet : fichiers]({% link cours/algorithme-code-theorie/code/projet-fichiers.md %})

### semaine 11

#### mercredi {#m-11}

1. [étude : recherche de sous-chaines]({% link cours/algorithme-code-theorie/algorithme/etude-recherche-sous-chaines.md %})

#### vendredi {#v-11}

1. [étude : alignement de séquences]({% link cours/algorithme-code-theorie/algorithme/etude-alignement-sequences.md %})
2. [projet : alignement de sequences]({% link cours/algorithme-code-theorie/code/projet-alignement-sequences.md %})

### semaine 12

Contrôle final : sur table.

Au programme :

* tout
* voir un peu plus

## tests

* [année 2021/2022]({% link cours/algorithme-code-theorie/exercices/2021-2022/index.md %})
* [annales]({% link cours/algorithme-code-theorie/exercices/index.md %})
