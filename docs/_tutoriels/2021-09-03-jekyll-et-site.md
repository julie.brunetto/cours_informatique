---
layout: page
title:  "Générateur de site statique jekyll"
tags: langage jekyll
---

Une introduction générateur de site statique [jekyll](https://jekyllrb.com/). On y y verra aussi comment est organisé et fonctionne ce [site de cours](https://github.com/FrancoisBrucker/cours_informatique), généré en jekyll.

<!--more-->

## Plan

> TBD
{: .note}

* installation
* markdown et header
* template et liquid
* plugins ruby
* option clean, serve, compile
* github actions
* css, sass et assets
* _posts et architechture du site
* collections

