---
layout: page
title:  "TC : graphes"
category: cours
---

## Introduction


Le tronc commun d'informatique de l'ecm comporte beaucoup de parties. Ici, nous présentons uniquement la partie graphe.

Chaque partie est organisée en 3 types d'enseignements :

* séance de cours (**C,M**) : magistral.
    > pour acquérir  des connaissances de base
* séance tableau (**ST**) : exercices fait sans machine, accompagné d'un prof ou d'un corrigé
    > pour travailler/acquérir des connaissances théoriques
* séance machine (**SM**) : exercices fait avec votre machine, accompagné d'un prof ou d'un corrigé
    > pour travailler/acquérir des connaissances pratiques

## Séances

### C, M :  graphes

Le support du cours d'[Introduction aux graphes]({% link cours/graphes/index.md %})

### ST : arbres

Le [sujet aux TD]({% link cours/graphes/arbres.md %}) qui vous fera découvrir par l'exemple le monde merveilleux des arbres.

### ST et SM : arbres de recherche

La séance d'autonomie sur [les arbres de recherche]({% link cours/graphes/arbre-de-recherche.md %})

### ST : chemin de poids min

Le [sujet de TD]({% link cours/graphes/chemins.md %}) qui nous fera cheminer dans les graphes.

### C, M :  flots

Cours sur les [flots]({% link cours/graphes/flots.md %}) pour se prendre pour un plombier.

### ST : flots

Les [applications des flots]({% link cours/graphes/flots-applications.md %}) là où on ne les attends pas.
