---
layout: page
title:  "Temps 2 digital·e : Analyse des données"
category: cours
authors:
  - François Brucker
  - Emmanuel Daucé
---

Cours en duo. Nous ne montrerons ici que les trois cours de François Brucker.

Vous aurez besoin d'une installation de jupyter notebook opérationnelle sur votre ordinateur. Une façon simple d'obtenir ça est d'installer [anaconda](https://www.anaconda.com/products/individual), qui est une distribution python à visée data science. Pour cela, vous pouvez suivre [ce tutoriel]({% link _tutoriels/python/2021-09-14-installation-anaconda.md %}).

1. [téléchargez le cours 1]({% link /assets/cours/ana_data/ana-data-1.zip %})
2. [téléchargez le cours 2]({% link /assets/cours/ana_data/ana-data-2.zip %})
3. [téléchargez le cours 3]({% link /assets/cours/ana_data/ana-data-3.zip %})

> Les cours sont téléchargés sous la forme d'un dossier compressé au format zip. Si vous n'avez pas de logiciel pour dézipper des fichiers et que vous êtes sous windows, vous pouvez installer [7zip](https://www.7-zip.org/download.html) (choisissez *64-bit Windows x64* comme système).
