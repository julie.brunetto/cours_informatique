---
layout: page
title:  "Temps 1 digital·e : Méthodes de développement (MD2)"
category: cours
---


## Prérequis

Les connaissances et les outils que vous devez avoir pour commencer le cours.

### ordinateur pour le développement

Vous devrez écrire du code et installer des applications durant ce cours. Vous devez donc avoir un ordinateur portable en état de marche et des connaissance minimale sur son fonctionnement.

Plus précisément, peu importe le système d'exploitation (Linux, Mac ou windows) vous devez :

* avoir un système sain. Dans le doute [faire une installation fraîche de tout votre système]({% link _tutoriels/systeme/2021-09-01-installation-ordinateur.md %})
* connaitre les bases d'un système d'exploitation, [les fichiers et les dossiers]({% link _tutoriels/systeme/fichiers-navigation.md %})
* avoir accès à un [terminal]({% link _tutoriels/systeme/2021-08-24-terminal.md %})

### bases en programmation

Vous devez avoir déjà utilisé un langage de programmation, idéalement python

## plan

### cours 1

On vous donne les clés d'un serveur [ovh](https://www.ovh.com/fr/) où vous serez une [plante aromatique](https://fr.wikipedia.org/wiki/Plante_aromatique). Ce serveur vous permettra de vous entrainer à mettre en production vos créations. On vous initiera également à la gestion du code source avec git et github.

1. [ssh et shell]({% link cours/ssh_et_shell/index.md %})
2. [git et github]({% link cours/git_et_github/index.md %})

> Une fois que vous aurez compris git/github et que vous arriverez à vous connecter sur l'`ovh1.ec-m.fr` sans soucis, inspirez vous du projet [numérologie]({% link cours/web/projets/numerologie/index.md %}) (faite le si vous ne l'avez jamais fait) pour mettre en production un site nécessitant la triplette html/css/js.
{: .note}

**achievement** : vous aurez un site sur l'internet ! Et ce se sera le votre à vous.

## à faire

Pour valider cette UEes vous aurez deux choses à faire.

### contribuer au site

On vous demande de contribuer à deux choses :

* améliorer une partie existante (que vous connaissiez déjà ou que vous venez d'apprendre)
* écrire une nouvelle page (ajout sur une techno existante ou une nouvelle techno)

> Le but est que vous appreniez quelque chose puis le restituiez sur le site.

### projet

Créez un site permettant de jouer à un jeu solo pnp ou à un jeu à plusieurs sur un seul écran. Il faut que le jeu soit libre de droit ou que vous puissiez l'utiliser sans le payer/pirater (certains jeux possèdent une version payante et une version gratuite, comme minirogue par exemple ou under a falling skies)

> Un demi-jeu n'est pas un jeu. On ne commencera à noter que lorsque le jeu fonctionnera (l'early access/démo technique/faites moi confiance ça va marcher un jour/[insert your excuse here] ne marchera pas). 
> Donc choisissez un jeu que vous pourrez implémenter en entier.
{: .attention}

* <https://www.youtube.com/results?search_query=solo+pnp+games> donne plein de résultats (même si j'admet c'est *un peu* de niche). Voir <https://www.youtube.com/c/VeselkoKelava/videos> (connait pas mais a l'air de faire plein de review), elle fait plein de jeu solo <https://www.youtube.com/channel/UCxvtBSy1zJCvuY2lUfKSltw/videos> (mais elle parle vraiment trop lentement) ou encore <https://www.youtube.com/channel/UC1ubZ0hz4amLG9NcKZR5eJg/videos>.
* Il existe plein de type de jeux différents en solo et en pnp, mais les *roll & write* seront peut-être les jeux les plus facile à créer :
  * <https://www.youtube.com/watch?v=gq53GGRrS1Q>
  * Bathysphere
  * Utopia Engine
  * <https://pnpfrance.wordpress.com/2020/10/20/k-day-kaiju-war/>
  * [full moon](https://witchwaygames.com/full-moon-the-beast-of-gevaudan/)
  * <https://boardgamegeek.com/boardgame/183571/deep-space-d-6/files>
  * <https://www.pnpparadise.com/set2/star-trek-the-dice-game>
  * under a falling skies (il existe une version pnp)
* jeu de plateau solo
  * <https://boardgamegeek.com/boardgame/148729/maquis/files> (bon jeu de placement)
* jdr sans mj : à plusieurs devant un écran unique
  * <https://www.cestpasdujdr.fr/for-the-queen/> (j'ai. C'est zarb mais c'est bien)
  * [zombie cinema](https://www.arkenstonepublishing.net/zombiecinema/) (j'ai une version en fr)
* listes
  * <https://boardgamegeek.com/boardgamefamily/19564/contests-solitaire-print-play>
  * <https://boardgamegeek.com/geeklist/284628/solo-pnp-games-less-50-ratings-are-great>
  * <https://www.reddit.com/r/soloboardgaming/comments/l42lk8/need_some_lists_for_reccomended_pnp_games_to/>
* en vrac
  * A4 quest
  * [your name here vs the argonauts](https://pnpfrance.wordpress.com/2018/08/21/your-name-here-and-the-argonauts/)
  * mini-rogue
  * guildhaven city (plein de cartes + plateau)
  * <https://pnpfrance.wordpress.com/2020/10/20/do-not-forsake-me-oh-my-darling/> doit être bien en virtuel
  * <http://tribuneludique.canalblog.com/archives/2019/04/20/37270520.html> ?

## old

> TBD : [vieux cours à remanier]({% link cours/dfs/index.md %})
{: .note}
